package com.example.recyclerviewkotlin

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.*
import com.example.recyclerviewkotlin.adapter.LanguageAdapter
import com.example.recyclerviewkotlin.interfaces.OnItemClickListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    val languages:ArrayList<String> = ArrayList()
    val logo:ArrayList<Int> = ArrayList()
    internal var languageListView:RecyclerView? = null
    internal var imageToggle:ImageView?=null
    internal var adapter:LanguageAdapter? = null
    internal var layoutManager: LayoutManager?=null
    internal var layoutType:Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        addLanguages()
        addLogo()
        languageListView?.layoutManager = getLayoutManager()
        adapter=LanguageAdapter(languages,logo,this)
        languageListView!!.setAdapter(adapter)

    }

    private fun getLayoutManager(): LayoutManager {
        if (layoutType==0){
            layoutManager=LinearLayoutManager(this)
        }
        else {
            layoutManager = GridLayoutManager(this, 2)
        }

        return layoutManager!!
    }
    private fun initViews() {
      imageToggle=findViewById(R.id.imgToggle)
      imageToggle!!.visibility= View.GONE;
      imageToggle!!.setBackgroundResource(R.drawable.ic_list)
      languageListView=findViewById(R.id.rv_List)
    }

    private fun addLanguages(){
        languages.add("Java")
        languages.add("Kotlin")
        languages.add("Angular")
        languages.add("Python")
        languages.add("Swift")
        languages.add("C-Sharp")
        languages.add("C-Plus-Plus")
        languages.add("C")
        languages.add("PhP")
    }
    private fun addLogo(){
        logo.add(R.drawable.ic_java)
        logo.add(R.drawable.ic_kotlin)
        logo.add(R.drawable.ic_angular)
        logo.add(R.drawable.ic_python)
        logo.add(R.drawable.ic_swift)
        logo.add(R.drawable.ic_c)
        logo.add(R.drawable.ic_cplus)
        logo.add(R.drawable.ic_csharp)
        logo.add(R.drawable.ic_php)
    }
}