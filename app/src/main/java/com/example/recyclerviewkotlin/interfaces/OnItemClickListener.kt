package com.example.recyclerviewkotlin.interfaces

import android.view.View

 public interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}