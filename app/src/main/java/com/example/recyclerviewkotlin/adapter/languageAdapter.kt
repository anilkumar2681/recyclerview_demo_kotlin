package com.example.recyclerviewkotlin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewkotlin.R.layout
import com.example.recyclerviewkotlin.interfaces.OnItemClickListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_language_row.view.*

class LanguageAdapter(
    val items: ArrayList<String>,
    val logos: ArrayList<Int>,
    val context: Context
) : RecyclerView.Adapter<LanguageAdapter.ViewHolder>() {

     var onItemClickListener:OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(layout.custom_language_row, parent, false)
       )
    }
    override fun getItemCount(): Int {
        return items.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val picasso = Picasso.get()

        picasso.load(logos.get(position))
            .into(holder.imgLogo)
        holder.tvlanguage?.text = items.get(position);

        holder.itemView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View) {
                if (onItemClickListener != null) {

                    onItemClickListener!!.onItemClick(view,position)
                }
            }

        })

    }
    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val imgLogo = itemView.img_logo
        val tvlanguage = itemView.tv_language
    }

    fun SetOnItemClickListener(onItemClickListener: OnItemClickListener) {
       this.onItemClickListener=onItemClickListener
    }

}

